#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <boost/filesystem.hpp>


std::vector<std::string> split(const std::string& strToSplit, const char& delimeter)
{
	std::stringstream ss(strToSplit);
	std::string item;
	std::vector<std::string> splittedStrings;
	while (std::getline(ss, item, delimeter))
	{
		splittedStrings.push_back(item);
	}
	return splittedStrings;
}

void addData(const std::string& currLog, std::vector< std::pair <std::string, std::string> >& logs, std::vector<std::pair<std::string, int>>& counter)
{
	const int COUNT_OF_LOGTYPES = 5, INDEX_OF_LOGTYPE = 3;
	std::string temp;
	temp = split(currLog, ';')[INDEX_OF_LOGTYPE];
	temp.erase(0, 1);
	for (int i = 0; i < COUNT_OF_LOGTYPES; ++i)
		if (counter.at(i).first == temp)
			counter.at(i).second++;
	logs.push_back(std::make_pair(temp, currLog));
}

void readFile(const std::string& path, std::vector< std::pair <std::string, std::string> >& logs, std::vector<std::pair<std::string, int>>& counter)
{
	std::string line, currLog;
	std::ifstream infile;
	bool isStart = false;
	infile.open(path);

	while (getline(infile, line))
	{
		if (line.substr(0, 2) == "~#")
		{
			isStart = true;
			if (currLog == "")
			{
				currLog = line;
			}
			else
			{
				addData(currLog, logs, counter);
				currLog = line;
			}
			continue;
		}
		if (isStart)
			currLog += line;
	}

	if (!currLog.empty())
	{
		addData(currLog, logs, counter);
	}

	infile.close();
}


void printLogs(std::vector< std::pair <std::string, std::string> >& logs, const std::string& tmp)
{
	std::cout << "\n" + tmp + "\n[\n";
	for (auto it : logs)
		if (it.first == tmp)
			std::cout << it.second << std::endl;
	std::cout << "]\n";
}

void searchFiles(const std::string& path, std::vector<std::string>& logFiles)
{
	boost::system::error_code ec;
	boost::filesystem::path offset_path(path);

	for (boost::filesystem::directory_iterator it(offset_path, ec), eit;
		it != eit;
		it.increment(ec)
		)
	{
		if (ec)
			continue;
		if (boost::filesystem::is_regular_file(it->path()))
		{
			if (it->path().extension().string() == ".log")
				logFiles.push_back(it->path().string());
		}
	}
}

int main(int argc, char* argv[])
{
	std::vector <std::pair<std::string, std::string> > logs;
	std::vector <std::pair<std::string, int> > counter = { {"ERROR", 0},{"WARN", 0},{"DEBUG", 0},{"INFO", 0},{"TRACE", 0} };
	std::vector <std::string> logFiles, toShow;

	for (int i = 0; i < argc; ++i)
	{
		std::string s = argv[i];
		int index = 0;
		for (const auto& pair : counter) {
			if (pair.first == s) {
				break;
			}
			index++;
		}
		if (index != counter.size())
		{
			toShow.push_back(s);
			continue;
		}
		searchFiles(s, logFiles);
	}
	for (int i = 0; i < logFiles.size(); ++i)
		readFile(logFiles.at(i), logs, counter);

	std::cout << "there is " << logs.size() << " logs" << std::endl;

	for (auto it : counter)
		std::cout << it.first << " -> " << it.second << std::endl;

	for (int i = 0; i < toShow.size(); ++i)
		printLogs(logs, toShow[i]);

	system("pause");
	return 0;
}